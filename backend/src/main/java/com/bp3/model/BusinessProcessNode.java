package com.bp3.model;

import com.bp3.Node;
import com.bp3.NodeType;
/**
 *
 * BusinessProcessNode
 *
 *
 * @author ChrisV_G
 * @since 04/28/2018
 *
 *
 * Change history
 * --------------------------------------------------
 * Autor             Fecha          Modificacion
 * ----------------- -------------- ------------------
 */
public class BusinessProcessNode implements Node {

  private String id;
  private String name;
  private NodeType nodeType;


  @Override
  public String getId() {
    return this.id;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public NodeType getType() {
    return this.nodeType;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public void setType(NodeType type) {
    this.nodeType = type;
  }


}
