package com.bp3.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Process
 *
 * @author ChrisV_G
 * @since 04/28/2018
 * <p>
 * <p>
 * Change history
 * --------------------------------------------------
 * Autor             Fecha          Modificacion
 * ----------------- -------------- ------------------
 */
public class Process implements Serializable {

  private List<BusinessProcessEdge> edges;
  private List<BusinessProcessNode> nodes;

  public Process() {
    this.edges = new ArrayList<>();
    this.nodes = new ArrayList<>();
  }

  public List<BusinessProcessEdge> getEdges() {
    return edges;
  }

  public void setEdges(List<BusinessProcessEdge> edges) {
    this.edges = edges;
  }

  public List<BusinessProcessNode> getNodes() {
    return nodes;
  }

  public void setNodes(List<BusinessProcessNode> nodes) {
    this.nodes = nodes;
  }
}
