package com.bp3.model;

import com.bp3.Edge;
/**
 *
 * BusinessProcessEdge
 *
 *
 * @author ChrisV_G
 * @since 04/28/2018
 *
 *
 * Change history
 * --------------------------------------------------
 * Autor             Fecha          Modificacion
 * ----------------- -------------- ------------------
 */
public class BusinessProcessEdge implements Edge {


  private String from;
  private String to;

  @Override
  public String getFrom() {
    return this.from;
  }

  @Override
  public String getTo() {
    return this.to;
  }

  @Override
  public void setFrom(String nodeId) {
    this.from = nodeId;
  }

  @Override
  public void setTo(String nodeId) {
    this.to = nodeId;
  }

}
