package com.bp3;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The different types of {@link Node}s within a BPM process diagram.
 */
public enum NodeType {
  @JsonProperty("Gateway")
  GATEWAY,
  @JsonProperty("End")
  END,
  @JsonProperty("HumanTask")
  HUMAN_TASK,
  @JsonProperty("ServiceTask")
  SERVICE_TASK,
  @JsonProperty("Start")
  START
}
