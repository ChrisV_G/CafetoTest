package com.bp3.business;

import com.bp3.NodeType;
import com.bp3.model.BusinessProcessEdge;
import com.bp3.model.BusinessProcessNode;
import com.bp3.model.Process;

import java.util.List;
import java.util.stream.Collectors;

/**
 * NodeManager
 *
 * @author ChrisV_G
 * @since 04/28/2018
 * <p>
 * <p>
 * Change history
 * --------------------------------------------------
 * Autor             Fecha          Modificacion
 * ----------------- -------------- ------------------
 */
public class NodeManager {

  private Process finalProcess;

  public NodeManager() {
    this.finalProcess = new Process();
  }

  /**
   * Method that removes the SERVICE_TASK nodes from a Process object
   * @param initialProcess
   * @return Process new object without SERVICE_TASK
   */
  public Process skipServicesTasks(Process initialProcess) {
    BusinessProcessNode initialNode = getInitialNode(initialProcess.getNodes());
    BusinessProcessEdge initialEdge = getEdge(initialProcess.getEdges(), initialNode.getId());
    this.finalProcess.getNodes().add(initialNode);
    removeServicesTasks(initialProcess, initialEdge);
    return this.finalProcess;
  }

  /**
   * Metodo que itera recursivamente un Process y remueve los elementos de tipo SERVICE_TASK
   * Method that removes recursively a Process object the SERVICE_TASK elements
   * <1> Given the Start edge, this method process the Process object looking for SERVICE_TASK elements </1>
   * <2>  After find the SERVICE_ELEMENTS, the method proceed to skip it and replace the edge
   *      item by the next element (it can not be a SERVICE_TASK element)</2>
   * <3> If the element is is a GATEWAY, the method proceed to find all the edge nodes and skip
   *     their SERVICE_TASK elements</3>
   * <4> if the element is a END element, it will be added to the final Object and the method will finish </4>
   * <5> if the element is not any of the elements mentioned, it will be added to the final Process object </5>
   * @param process that will be processed
   * @param edge initial
   */
  public void removeServicesTasks(Process process, BusinessProcessEdge edge) {
    BusinessProcessNode node = getNode(process.getNodes(), edge.getTo());

    if (node.getType().equals(NodeType.END)) {
      existNode(node);
      existEdge(edge);
    } else if (node.getType().equals(NodeType.GATEWAY)) {
      existEdge(edge);
      existNode(node);
      getEdges(process.getEdges(), edge.getTo()).stream().forEach(tempEdge -> {
        if (!existNode(this.finalProcess.getNodes(), getNode(process.getNodes(), tempEdge.getTo()))) {
          existEdge(tempEdge);
          removeServicesTasks(process, tempEdge);
        } else {
          existEdge(tempEdge);
        }
      });
    } else if (!node.getType().equals(NodeType.SERVICE_TASK)) {
      existNode(node);
      BusinessProcessEdge actualEdge = getEdge(process.getEdges(), node.getId());
      if (!getNode(process.getNodes(), edge.getFrom()).equals(NodeType.SERVICE_TASK)) {
        existEdge(getEdge(process.getEdges(), edge.getFrom()));
      }
      removeServicesTasks(process, actualEdge);
    } else {
      BusinessProcessEdge actualEdge = getEdge(process.getEdges(), node.getId());
      edge.setTo(actualEdge.getTo());
      existEdge(edge);
      removeServicesTasks(process, edge);
    }
  }

  /**
   * Get the initial node from a List<BusinessProcessNode> nodes (START type)
   * @param nodes
   * @return BusinessProcessNode
   */
  public BusinessProcessNode getInitialNode(List<BusinessProcessNode> nodes) {
    return nodes.stream().filter(e -> e.getType().equals(NodeType.START)).findAny().orElse(null);
  }

  /**
   * Gets an Edge from a given id comparing against "From" item
   * @param edges
   * @param match
   * @return
   */
  public BusinessProcessEdge getEdge(List<BusinessProcessEdge> edges, String match) {
    return edges.stream().filter(e -> e.getFrom().equals(match)).findAny().orElse(null);
  }

  /**
   * Gets a node from a given id
   * @param nodes
   * @param match
   * @return
   */
  public BusinessProcessNode getNode(List<BusinessProcessNode> nodes, String match) {
    return nodes.stream().filter(e -> e.getId().equals(match)).findAny().orElse(null);
  }

  /**
   * Gets all the Edges from a given id(From)
   * @param nodes
   * @param match
   * @return List<BusinessProcessEdge>
   */
  public List<BusinessProcessEdge> getEdges(List<BusinessProcessEdge> nodes, String match) {
    return nodes.stream().filter(e -> e.getFrom().equals(match)).collect(Collectors.toList());
  }

  /**
   * Validate if the given node is a SERVICE_TASK element
   * @param nodes
   * @param edge
   * @return boolean
   */
  public boolean isServiceTask(List<BusinessProcessNode> nodes, BusinessProcessEdge edge) {
    BusinessProcessNode node = getNode(nodes, edge.getTo());
    return node.getType().equals(NodeType.SERVICE_TASK);

  }

  /**
   * Validate if the given node is a END element
   * @param node
   */
  private void existNode(BusinessProcessNode node) {
    if (!existNode(this.finalProcess.getNodes(), node))
      this.finalProcess.getNodes().add(node);
  }

  /**
   * Validate if the given Edge exist in the FinalProcess object
   * @param edge
   */
  private void existEdge(BusinessProcessEdge edge) {
    if (!exist(this.finalProcess.getEdges(), edge))
      this.finalProcess.getEdges().add(edge);
  }

  /**
   * Get the subsequent Edge element
   * @param edges
   * @param edge
   * @return
   */
  public BusinessProcessEdge getNextEdge(List<BusinessProcessEdge> edges, BusinessProcessEdge edge) {
    return getEdge(edges, edge.getTo());
  }

  /**
   * Validate if the given Edge exist in the List<BusinessProcessEdge> edges list
   * @param edges
   * @param edge
   * @return
   */
  public boolean exist(List<BusinessProcessEdge> edges, BusinessProcessEdge edge) {
    return edges.stream().anyMatch(t -> t.getFrom().equals(edge.getFrom()) && t.getTo().equals(edge.getTo()));
  }

  /**
   * Validate if the given Node exist in the List<BusinessProcessNode> nodes list
   * @param nodes
   * @param node
   * @return
   */
  public boolean existNode(List<BusinessProcessNode> nodes, BusinessProcessNode node) {
    return nodes.stream().anyMatch(t -> t.getId().equals(node.getId()));
  }
}
