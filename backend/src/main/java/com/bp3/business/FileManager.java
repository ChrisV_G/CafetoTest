package com.bp3.business;

import com.bp3.model.Process;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * FileManager
 *
 * @author ChrisV_G
 * @since 04/28/2018
 * <p>
 * <p>
 * Change history
 * --------------------------------------------------
 * Autor             Fecha          Modificacion
 * ----------------- -------------- ------------------
 */
public class FileManager {
  private static Logger logger = LoggerFactory.getLogger(FileManager.class);

  /**
   * Read a file from a given location and convert it content from JSON to a Process Object
   *
   * @param fileLocation
   * @return process
   * @throws Exception
   */
  public Process readFile(String fileLocation) throws Exception {
    try {
      ClassLoader classLoader = getClass().getClassLoader();
      File file = new File((fileLocation));
      String data = FileUtils.readFileToString(file);
      return jsonToJavaProcessObject(data);
    } catch (IOException e) {
      logger.error("Error trying to read the file: " + e.getLocalizedMessage());
      throw e;
    } catch (Exception e) {
      logger.error(e.getLocalizedMessage());
      throw e;
    }
  }

  /**
   * Convert a Process object to JSON, to later be written in a given location
   *
   * @param fileLocation
   * @param processObject
   */
  public void writeFile(String fileLocation, Process processObject) throws IOException {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.writeValue(new File(fileLocation), processObject);
    } catch (IOException e) {
      logger.error("Error trying to write the file: "+e.getLocalizedMessage());
      throw e;
    }catch (Exception e) {
      logger.error(e.getLocalizedMessage());
      throw e;
    }

  }

  /**
   * Convert a String JSON format into a Process object
   *
   * @param jsonObject
   * @return Process
   * @throws Exception
   */
  public Process jsonToJavaProcessObject(String jsonObject) throws Exception {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.readValue(jsonObject, Process.class);
    } catch (IOException e) {
      logger.error(e.getLocalizedMessage());
      throw new IOException("Error parsing JSON: " + e.getLocalizedMessage());
    } catch (Exception e) {
      logger.error(e.getLocalizedMessage());
      throw e;
    }

  }
}
