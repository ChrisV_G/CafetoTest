import static org.junit.Assert.assertNotNull;

import com.bp3.NodeType;
import com.bp3.business.FileManager;
import com.bp3.business.NodeManager;
import com.bp3.model.BusinessProcessNode;
import com.bp3.model.Process;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;

public class FileManagerTest {
  private static Logger logger = LoggerFactory.getLogger(AppTest.class);

  private final FileManager fileManager;

  public FileManagerTest() {
    fileManager = new FileManager();
  }

  @Test
  public void testReadFile() {
    try {
      URL url = this.getClass().getClassLoader().getResource("/");
      Process process = fileManager.readFile(getClass().getClassLoader().getResource("1-simple-process-test.json").getFile());
      assertNotNull("The object can not be null", process.getNodes());
    } catch (Exception e) {
      assertNotNull("Error trying to execute the test", null);
      logger.error(e.getLocalizedMessage());
    }
  }

  @Test
  public void testWriteFile() {
    try {
      File file = new File(this.getClass().getResource("1-simple-process-test.json").getFile());
      fileManager.writeFile(file.getAbsolutePath().replace("-test", "-output"), new Process());
    } catch (Exception e) {
      assertNotNull("Error writing the file ", null);
      logger.error(e.getLocalizedMessage());
    }
  }

  @Test
  public void testJSONToProcessObject() {
    try {
      String jsonObject="{\n" +
          "  \"nodes\": [\n" +
          "    {\n" +
          "      \"id\": 0,\n" +
          "      \"name\": \"Start\",\n" +
          "      \"type\": \"Start\"\n" +
          "    } \n" +
          "  ],\n" +
          "  \"edges\": [\n" +
          "    {\n" +
          "      \"from\": 0,\n" +
          "      \"to\": 1\n" +
          "    } \n" +
          "  ]\n" +
          "}\n";
      Process process=fileManager.jsonToJavaProcessObject(jsonObject);
      assertNotNull(process);
    } catch (Exception e) {
      assertNotNull("Error writing the file ", null);
      logger.error(e.getLocalizedMessage());
    }
  }
}
