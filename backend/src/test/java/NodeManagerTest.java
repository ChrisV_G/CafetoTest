import com.bp3.NodeType;
import com.bp3.business.FileManager;
import com.bp3.business.NodeManager;
import com.bp3.model.BusinessProcessEdge;
import com.bp3.model.BusinessProcessNode;
import com.bp3.model.Process;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

import static org.junit.Assert.assertNotNull;

public class NodeManagerTest {
  private static Logger logger = LoggerFactory.getLogger(AppTest.class);
  private final NodeManager nodeManager ;
  private final FileManager fileManager;
  private  BusinessProcessNode testNode;
  private  BusinessProcessEdge testEdge;
  private Process process;

  public NodeManagerTest() {
    fileManager = new FileManager();
    nodeManager = new NodeManager();
    testNode=new BusinessProcessNode();
    testEdge= new BusinessProcessEdge();
    testEdge.setFrom("0");
    testEdge.setTo("1");
    testNode.setId("2");
    testNode.setName("B");
    testNode.setType(NodeType.HUMAN_TASK);
    try {
      URL url = this.getClass().getClassLoader().getResource("/");
      this.process = fileManager.readFile(getClass().getClassLoader().getResource("1-simple-process-test.json").getFile());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  @Test
  public void testGetInitialNode() {
    try {
      BusinessProcessNode node=nodeManager.getInitialNode(this.process.getNodes());
      assert(node.getType().equals(NodeType.START));
    } catch (Exception e) {
      assertNotNull("Error trying to execute the test", null);
      logger.error(e.getLocalizedMessage());
    }
  }


  @Test
  public void testIsServiceTask() {
      //the edge with id 0 has a SERVICE_TASK node
      assert(nodeManager.isServiceTask(process.getNodes(),process.getEdges().get(0)));
  }
  @Test
  public void testExistEdge() {
    //the edge with id 0 has a SERVICE_TASK node
    assert(nodeManager.exist(process.getEdges(),testEdge));
  }
  @Test
  public void testExistNode() {
    //the edge with id 0 has a SERVICE_TASK node
    assert(nodeManager.existNode(process.getNodes(),testNode));
  }
}
