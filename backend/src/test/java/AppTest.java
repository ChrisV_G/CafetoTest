import static org.junit.Assert.assertNotNull;

import com.bp3.NodeType;
import com.bp3.business.FileManager;
import com.bp3.business.NodeManager;
import com.bp3.model.BusinessProcessNode;
import com.bp3.model.Process;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;

/**
 *
 * Process
 *
 *
 * @author ChrisV_G
 * @since 04/28/2018
 *
 *
 * Change history
 * --------------------------------------------------
 * Autor             Fecha          Modificacion
 * ----------------- -------------- ------------------
 */
public class AppTest {
  private static Logger logger = LoggerFactory.getLogger(AppTest.class);
  private final NodeManager nodeManager ;
  private final FileManager fileManager;

  public AppTest() {
     nodeManager = new NodeManager();
     fileManager = new FileManager();
  }

  @Test
  public void testSimpleProcess() {
    try {
      URL url = this.getClass().getClassLoader().getResource("/");
      Process process = fileManager.readFile(getClass().getClassLoader().getResource("1-simple-process-test.json").getFile());
      Process finalProcess = nodeManager.skipServicesTasks(process);
      assertNotNull("The object can not be null", finalProcess.getNodes());
      for (BusinessProcessNode o : finalProcess.getNodes()) {
        assert(!o.getType().equals(NodeType.SERVICE_TASK));
      }
      File file = new File(this.getClass().getResource("1-simple-process-test.json").getFile());
      logger.info(file.getAbsolutePath().replace("-test","-output"));
      fileManager.writeFile(file.getAbsolutePath().replace("-test","-output"),finalProcess);
    } catch (Exception e) {
      assertNotNull("Error trying to execute the test",null);
      logger.error(e.getLocalizedMessage());
    }
  }

  @Test
  public void testMultipleHumanServices() {
    try {
      Process process = fileManager.readFile(getClass().getClassLoader().getResource("2-multiple-human-services-test.json").getFile());
      Process finalProcess = nodeManager.skipServicesTasks(process);
      assertNotNull("The object can not be null", finalProcess.getNodes());
      for (BusinessProcessNode o : finalProcess.getNodes()) {
        assert(!o.getType().equals(NodeType.SERVICE_TASK));
      }
      File file = new File(this.getClass().getResource("2-multiple-human-services-test.json").getFile());
      logger.info(file.getAbsolutePath().replace("-test","-output"));
      fileManager.writeFile(file.getAbsolutePath().replace("-test","-output"),finalProcess);
    } catch (Exception e) {
      assertNotNull("Error trying to execute the test",null);
      logger.error(e.getLocalizedMessage());
    }
  }

  @Test
  public void testBranchingProcess() {
    try {
      Process process = fileManager.readFile(getClass().getClassLoader().getResource("3-branching-process-test.json").getFile());
      Process finalProcess = nodeManager.skipServicesTasks(process);
      assertNotNull("The object can not be null", finalProcess.getNodes());
      for (BusinessProcessNode o : finalProcess.getNodes()) {
        assert(!o.getType().equals(NodeType.SERVICE_TASK));
      }
      File file = new File(this.getClass().getResource("3-branching-process-test.json").getFile());
      logger.info(file.getAbsolutePath().replace("-test","-output"));
      fileManager.writeFile(file.getAbsolutePath().replace("-test","-output"),finalProcess);
    } catch (Exception e) {
      assertNotNull("Error trying to execute the test",null);
      logger.error(e.getLocalizedMessage());
    }
  }

  @Test
  public void testRecursiveBranching() {
    try {
      Process process = fileManager.readFile(getClass().getClassLoader().getResource("4-recursive-branching-process-test.json").getFile());
      Process finalProcess = nodeManager.skipServicesTasks(process);
      assertNotNull("The object can not be null", finalProcess.getNodes());
      for (BusinessProcessNode o : finalProcess.getNodes()) {
        assert(!o.getType().equals(NodeType.SERVICE_TASK));
      }
      File file = new File(this.getClass().getResource("4-recursive-branching-process-test.json").getFile());
      logger.info(file.getAbsolutePath().replace("-test","-output"));
      fileManager.writeFile(file.getAbsolutePath().replace("-test","-output"),finalProcess);
    } catch (Exception e) {
      assertNotNull("Error trying to execute the test",null);
      logger.error(e.getLocalizedMessage());
    }
  }

}
